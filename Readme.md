# packingPlacementTest

### 介绍
对多种装箱排样算法进行实现

### 软件架构
笔者python环境由anaconda提供，调试时采用python3.7环境

算法实现对应的文件如下：

文件  | 算法  
 ---- | ----- 
 lowest_horizontal_line.py  | 最低水平线算法
 lowest_horizontal_line_search.py  | 最低水平线搜索算法（加入旋转操作） 

### 准备工作

安装anaconda并创建python3环境，或直接按照python3

### 使用说明

直接运行相应的python文件即可
